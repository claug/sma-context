package ro.upt.sma.context.activity

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.google.android.gms.location.ActivityRecognition
import com.google.android.gms.location.ActivityRecognitionClient

class ActivityRecognitionHandler(private val context: Context) {

    private val client: ActivityRecognitionClient = ActivityRecognition.getClient(context)

    @SuppressLint("MissingPermission")
    fun registerPendingIntent(): PendingIntent {

        // TODO 4: Create a pending intent for [ActivityRecognitionService],
        //  register that intent for updates to activity recognition client
        //  and return that pending intent.
        val intent: Intent
        val pendingIntent: PendingIntent = null!!

        return pendingIntent
    }

    @SuppressLint("MissingPermission")
    fun unregisterPendingIntent(pendingIntent: PendingIntent) {
        client.removeActivityUpdates(pendingIntent)
    }

}
